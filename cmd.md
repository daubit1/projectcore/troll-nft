Set env variables for less verbosing commands.
You can run the address.bat in Windows which fetches the current address in the 31337 network, which is the local testnet
You can also manually insert the values in here.
Deploy the contract to the local testnet with: 
<br><br>``npm run deploy:dev`` <br><br>
``ADDR = ""`` <br>
``FLAGS = --network localhost --address %ADDR%``

Add a collection to the contract <br>
``npx hardhat addCollection %FLAGS% --name Kartal`` 

Fetch the name of a collection id <br>
``npx hardhat getCollection %FLAGS% --id 1 ``

Add URIs to a collection specified by its id <br>
``npx hardhat addURIs %FLAGS% --id 1 --path data/dummy.json ``

Return the totalsupply of URIs a collection by id has <br>
``npx hardhat totalSupply %FLAGS% --id 1``