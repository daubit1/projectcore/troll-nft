// SPDX-License-Identifier: CC-BY-NC-4.0

pragma solidity 0.8.15;

import "./ERC721TradeableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";

/**
 * @title TrollToken
 * TrollToken - a contract for my non-fungible creatures.
 */
contract TrollToken is Initializable, ERC721TradeableUpgradeable {
    using CountersUpgradeable for CountersUpgradeable.Counter;

    // collection => uri id => uri
    mapping(uint256 => mapping(uint256 => string)) private _uris;

    // collection => address => amount
    mapping(uint256 => mapping(address => uint8)) private _mintAmount;

    // collection => name
    mapping(uint256 => string) private _collections;

    // collection => address => how many tokens the address can mint/has minted from whitelist
    mapping(uint256 => mapping(address => uint256)) private _whitelistMintAmount;

    // collection => how many tokens any address can mint
    mapping(uint256 =>  uint256) private _freeMintAmount;

    // collection => timestamp for whitelist mint start
    mapping(uint256 => uint256) private _whitelistMintStart;

    // collection => timestamp for whitelist free start
    mapping(uint256 => uint256) private _freeMintStart;

    // collection => token amount
    mapping(uint256 => CountersUpgradeable.Counter) private _totalURIs;

    string private _contractURI;

    CountersUpgradeable.Counter private _collectionIdCounter;

    /**
     * @dev the initializer sets the proxyRegistryAddress and the contract URI of the contract.
     * @param _proxyRegistryAddress representing the address of the OpenSea Proxy Registry
     * @param contractURI_ representing the CDI of the contract.
     */
    function initialize(
        address _proxyRegistryAddress,
        string memory contractURI_
    ) public initializer {
        __ERC721Tradeable_init("TrollToken", "TT", _proxyRegistryAddress);
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the base URI for every URI.
     */
    function baseTokenURI() public pure override returns (string memory) {
        return "ipfs://";
    }

    /**
     * @dev Allows the owner to set a new contract URI
     *
     * @param contractURI_ representing the URI of the meta data of the contract.
     */
    function setContractURI(string memory contractURI_)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the contract URI.
     */
    function contractURI() external view returns (string memory) {
        return string(abi.encodePacked(baseTokenURI(), _contractURI));
    }

    /**
     * @dev Adds an URI to a collection
     *
     * @param collectionId id of the collection to add to
     * @param uri meta data
     */
    function addURI(uint256 collectionId, string memory uri)
        public
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(bytes(uri).length != 0, "Error: Invalid string!");
        uint256 totalURIs = totalSupply(collectionId);
        _uris[collectionId][totalURIs] = uri;
        _totalURIs[collectionId].increment();
    }

    /**
     * @dev Adds an URI to a collection
     *
     * @param collectionId id of the collection to add to
     * @param uris array of meta data
     */
    function addURIs(uint256 collectionId, string[] calldata uris)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(uris.length != 0, "Error: Invalid array!");
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: No collection"
        );
        for (uint256 i; i < uris.length; i++) {
            addURI(collectionId, uris[i]);
        }
    }

    /**
     * @dev Removes an URI from a collection
     *
     * @param collectionId id of the collection to add to
     * @param index uint256 index of the URI to remove
     */
    function removeURI(uint256 collectionId, uint256 index)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _remove(collectionId, index);
    }

    /**
     * @dev Mints a token with a random URI
     *
     * @param to address of the future owner of the token
     */
    function mint(uint256 collectionId, address to) external {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        require(block.timestamp >= _freeMintStart[collectionId], "Error: free mint has not started yet");
        require(
            _mintAmount[collectionId][msg.sender] < _freeMintAmount[collectionId],
            "Error: Cannot mint anymore!"
        );
        uint256 totalURIs = totalSupply(collectionId);
        require(totalURIs > 0, "Error: No NFTs left to mint");
        uint256 randIndex = _randomIndex(totalURIs);
        string memory uri = _uris[collectionId][randIndex];
        _remove(collectionId, randIndex);
        _mintTo(to, uri);
        _mintAmount[collectionId][msg.sender]++;
    }

    /**
     * @dev Mints a token with a random URI
     * @param to address of the future owner of the token
     */
    function whitelistMint(uint256 collectionId, address to) external {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        require(block.timestamp >= _whitelistMintStart[collectionId], "Error: whitelist mint has not started yet");
        require(
            _whitelistMintAmount[collectionId][msg.sender] > 0,
            "Error: Cannot mint anymore!"
        );
        uint256 totalURIs = totalSupply(collectionId);
        require(totalURIs > 0, "Error: No NFTs left to mint");
        uint256 id = _randomIndex(totalURIs);
        string memory uri = _uris[collectionId][id];
        _remove(collectionId, id);
        _mintTo(to, uri);
        _whitelistMintAmount[collectionId][msg.sender]--;
    }

    /**
     * @dev Adds a new collection to mint from
     *
     * @param collection name of the new collection
     */
    function addCollection(string memory collection, uint256 whitelistMintStart, uint256 freeMintStart, uint256 freeMintAmount)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _collectionIdCounter.increment();
        uint256 collectionId = _collectionIdCounter.current();
        _collections[collectionId] = collection;
        _totalURIs[collectionId].increment();
        _whitelistMintStart[collectionId] = whitelistMintStart;
        _freeMintStart[collectionId] = freeMintStart;
        _freeMintAmount[collectionId] = freeMintAmount;
    }

    function addToWhitelist(uint256 collectionId, address[] memory addresses, uint256[] memory amounts) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(addresses.length == amounts.length, "Error: Array lengths mismatch");
        for (uint256 index = 0; index < addresses.length; index++) {
            _whitelistMintAmount[collectionId][addresses[index]] = amounts[index];
        }
    }

    /**
     * @dev Returns the collection name
     *
     * @param collectionId id of the collection
     */
    function getCollection(uint256 collectionId)
        external
        view
        returns (string memory)
    {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: No collection"
        );
        return _collections[collectionId];
    }

    /**
     * @dev Sets a new collection mint limit
     *
     * @param collectionId id of the collection
     * @param amount uint256
     */
    function setFreeMintLimit(uint256 collectionId, uint256 amount)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _freeMintAmount[collectionId] = amount;
    }

    /**
     * @dev Returns mint limit of the specified collection
     *
     * @param collectionId id of the collection
     */
    function getFreeMintLimit(uint256 collectionId)
        external
        view
        returns (uint256)
    {
        return _freeMintAmount[collectionId];
    }

    /**
     * @dev Returns the amount of token to mint in the specified collection
     *
     * @param collectionId id of the collection
     */
    function totalSupply(uint256 collectionId) public view returns (uint256) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: No collection"
        );
        return _totalURIs[collectionId].current() - 1;
    }

    /**
     * @dev Helper function for removing an uri
     *
     * @param collectionId id of the collection to add to
     * @param index uint256 index of the URI to remove
     */
    function _remove(uint256 collectionId, uint256 index) internal {
        uint256 lastIndex = totalSupply(collectionId) - 1;
        require(0 <= index && index <= lastIndex, "Error: Out of bounds!");
        _uris[collectionId][index] = _uris[collectionId][lastIndex];
        _totalURIs[collectionId].decrement();
    }

    /**
     * @dev Pseudo-random generator.
     *
     * @param length uint256
     */
    function _randomIndex(uint256 length) internal view returns (uint256) {
        bytes32 hashValue = keccak256(
            abi.encodePacked(
                msg.sender,
                blockhash(block.number - 1),
                block.number,
                block.timestamp,
                block.difficulty
            )
        );
        return uint256(hashValue) % length;
    }

    function getWhitelistSlots(uint256 collectionId, address addr) external view returns(uint256) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        return _whitelistMintAmount[collectionId][addr];
    }

    function getWhitelistMintStart(uint256 collectionId) external view returns(uint256) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        return _whitelistMintStart[collectionId];
    }

    function getFreeMintStart(uint256 collectionId) external view returns(uint256) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        return _freeMintStart[collectionId];
    }

    function setWhitelistMintStart(uint256 collectionId, uint256 timestamp) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        _whitelistMintStart[collectionId] = timestamp;
    }

    function setFreeMintStart(uint256 collectionId, uint256 timestamp) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Error: Invalid collection id!"
        );
        _freeMintStart[collectionId] = timestamp;
    }
}
