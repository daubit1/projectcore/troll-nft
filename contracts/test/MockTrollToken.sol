// SPDX-License-Identifier: CC-BY-NC-4.0

pragma solidity 0.8.15;

import "../ERC721TradeableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";

/**
 * @title TrollToken
 * TrollToken - a contract for my non-fungible creatures.
 */
contract MockTrollToken is Initializable, ERC721TradeableUpgradeable {
    using CountersUpgradeable for CountersUpgradeable.Counter;

    mapping(uint256 => mapping(uint256 => string)) private _uris;

    // collection => address => amount
    mapping(uint256 => mapping(address => uint8)) private _mintAmount;

    // collection => name
    mapping(uint256 => string) private _collections;

    // collection => address => how many tokens the address can mint/has minted from whitelist
    mapping(uint256 => mapping(address => uint256)) private _whitelistMintAmount;

    // collection => how many tokens any address can mint
    mapping(uint256 =>  uint256) private _freeMintAmount;

    // collection => timestamp for whitelist mint start
    mapping(uint256 => uint256) private _whitelistMintStart;

    // collection => timestamp for whitelist free start
    mapping(uint256 => uint256) private _freeMintStart;

    // collection => token amount
    mapping(uint256 => CountersUpgradeable.Counter) private _totalURIs;

    string private _contractURI;

    CountersUpgradeable.Counter private _collectionIdCounter;

    /**
     * @dev the initializer sets the proxyRegistryAddress and the contract URI of the contract.
     * @param _proxyRegistryAddress representing the address of the OpenSea Proxy Registry
     * @param contractURI_ representing the CDI of the contract.
     */
    function initialize(
        address _proxyRegistryAddress,
        string memory contractURI_
    ) public initializer {
        __ERC721Tradeable_init("MockTrollToken", "MTT", _proxyRegistryAddress);
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the base URI for every URI.
     */
    function baseTokenURI() public pure override returns (string memory) {
        return "ipfs://";
    }

    /**
     * @dev Allows the proxy owner to upgrade the current version of the proxy.
     * @param contractURI_ representing the address of the new implementation to be set.
     */
    function setContractURI(string memory contractURI_)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the contract URI.
     */
    function contractURI() external view returns (string memory) {
        return string(abi.encodePacked(baseTokenURI(), _contractURI));
    }

    function addURI(uint256 collectionId, string memory uri)
        public
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(bytes(uri).length != 0, "Invalid string!");
        uint256 totalURIs = _totalURIs[collectionId].current() - 1;
        _uris[collectionId][totalURIs] = uri;
        _totalURIs[collectionId].increment();
    }

    function addURIs(uint256 collectionId, string[] calldata uris)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(uris.length != 0, "Invalid array!");
        for (uint256 i; i < uris.length; i++) {
            addURI(collectionId, uris[i]);
        }
    }

    function removeURI(uint256 collectionId, uint256 id)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _remove(collectionId, id);
    }

    /**
     * @dev Mints a token with a random URI
     * @param to address of the future owner of the token
     */
    function mint(uint256 collectionId, address to) external {
        require(
            collectionId > 0 && collectionId <= _collectionIdCounter.current(),
            "Invalid collection id!"
        );
        require(
            _mintAmount[collectionId][msg.sender] < 2,
            "Error: Cannot mint anymore!"
        );
        uint256 totalURIs = _totalURIs[collectionId].current();
        uint256 id = _randomIndex(totalURIs);
        string memory uri = _uris[collectionId][id];
        _remove(collectionId, id);
        _mintTo(to, uri);
        _mintAmount[collectionId][msg.sender]++;
    }

    /**
     * @dev Adds a new collection to mint from
     * @param collection name of the new collection
     */
    function addCollection(string memory collection)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _collectionIdCounter.increment();
        uint256 collectionId = _collectionIdCounter.current();
        _collections[collectionId] = collection;
        _totalURIs[collectionId].increment();
    }

    /**
     * @dev Returns the collection name
     * @param collectionId id of the collection
     */
    function getCollection(uint256 collectionId)
        external
        view
        returns (string memory)
    {
        return _collections[collectionId];
    }

    /**
     * @dev Returns the amount of token to mint in the specified collection
     * @param collectionId id of the collection
     */
    function totalSupply(uint256 collectionId) external view returns (uint256) {
        return _totalURIs[collectionId].current() - 1;
    }

    function _remove(uint256 collectionId, uint256 i) internal {
        uint256 totalURIs = _totalURIs[collectionId].current();
        require(0 <= i && i < totalURIs, "Error: Out of bounds!");
        _uris[collectionId][i] = _uris[collectionId][totalURIs - 1];
        _uris[collectionId][totalURIs - 1] = "";
        _totalURIs[collectionId].decrement();
    }

    /**
     * @dev Pseudo-random generator.
     *
     * @param length uint256
     */
    function _randomIndex(uint256 length) internal view returns (uint256) {
        bytes32 hashValue = keccak256(
            abi.encodePacked(
                msg.sender,
                blockhash(block.number - 1),
                block.number,
                block.timestamp,
                block.difficulty
            )
        );
        return uint256(hashValue) % length;
    }
}
