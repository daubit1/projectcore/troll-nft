import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import {
  addCollection,
  addToWhitelist,
  addURIs,
  batchMint,
  donate,
  getCollection,
  setContractURI,
  setPrivateSale,
  setPublicSale,
  totalSupply,
} from "./scripts/tasks";

dotenv.config();

const MNEMONIC = process.env.MNEMONIC;
const ALCHEMY_KEY_MAINNET = process.env.ALCHEMY_KEY_MAINNET;
const ALCHEMY_KEY_TESTNET = process.env.ALCHEMY_KEY_TESTNET;
const ALCHEMY_ETH = process.env.ALCHEMY_ETH;

const mumbaiNodeUrl = `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_KEY_TESTNET}`;
const polygonNodeUrl = `https://polygon-mainnet.g.alchemy.com/v2/${ALCHEMY_KEY_MAINNET}`;
const ethUrl = `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_ETH}`;

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

task("addCollection", "Add a new collection to the contract", addCollection)
  .addParam("address")
  .addParam("name")
  .addParam("ws")
  .addParam("fs")
  .addParam("a");

task("batchMint", "mint", batchMint)
  .addParam("address")
  .addParam("batchid")
  .addParam("batch");

task("setContractURI", "mint", setContractURI)
  .addParam("address")
  .addParam("cid");

task(
  "getCollection",
  "Fetch the collection name of the contract",
  getCollection
)
  .addParam("address")
  .addParam("id");

task("addURIs", "Add a list of URIs to the contract", addURIs)
  .addParam("address", "Contract address")
  .addParam("id")
  .addParam("path");

task(
  "totalSupply",
  "Fetch the total supply of URIs from a collection",
  totalSupply
)
  .addParam("address", "Contract address")
  .addParam("id");

task("donate", "Receives some ether for testing", donate).addParam(
  "address",
  "Donatee"
);

task("addWhitelist", "Add a whitelist to the contract", addToWhitelist)
  .addParam("address", "Contract address")
  .addParam("id")
  .addParam("path");

task("setPrivateSale", "Set timestamp for private sale", setPrivateSale)
  .addParam("address")
  .addParam("t")
  .addParam("id");

task("setPublicSale", "Set timestamp for public sale", setPublicSale)
  .addParam("address")
  .addParam("t")
  .addParam("id");

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.15",
    settings: {
      optimizer: {
        enabled: true,
        runs: 1000,
      },
    },
  },

  networks: {
    mumbai: { url: mumbaiNodeUrl, accounts: { mnemonic: MNEMONIC } },
    polygon: { url: polygonNodeUrl, accounts: { mnemonic: MNEMONIC } },
    polygonPublic: {
      url: "https://polygon-rpc.com",
      accounts: { mnemonic: MNEMONIC },
    },
    eth: { url: ethUrl, accounts: { mnemonic: MNEMONIC } },
    ethPublic: {
      url: "https://eth.public-rpc.com",
      accounts: { mnemonic: MNEMONIC },
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: {
      polygon: process.env.POLYSCAN_KEY!,
      polygonMumbai: process.env.POLYSCAN_KEY!,
    },
  },
};
export default config;
