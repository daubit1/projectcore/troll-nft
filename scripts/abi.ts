import { writeFileSync } from "fs";
import * as TrollToken from "../artifacts/contracts/TrollToken.sol/TrollToken.json";

const abi = `export const CONTRACT_ABI = ${JSON.stringify(
  TrollToken.abi,
  null,
  2
)}`;

writeFileSync("data/abi.ts", abi);

console.log("Successfully added abi!");
