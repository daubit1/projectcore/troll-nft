import dotenv from "dotenv";
import { ethers } from "hardhat";
dotenv.config();

const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const TOKEN_CID = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";
const NFT_CONTRACT_ADDRESS = "0x0e1cfB180319058cA1B1A757474112a35662fd35";


async function main() {
  const TrollToken = await ethers.getContractFactory("TrollToken");
  const troll = TrollToken.attach(NFT_CONTRACT_ADDRESS);

  const recipients = Array(200).fill(OWNER_ADDRESS);
  const tokenURIs = Array(200).fill(TOKEN_CID);
  console.log("Sending tx...");
  const tx = await troll.batchMint(recipients, tokenURIs);
  console.log({ tx });
}

main()
  .then(() => process.exit(0))
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
