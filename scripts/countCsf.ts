import { readFile } from "fs/promises";
import { AddressProcessed } from "./types/types";

async function main() {
  const file: AddressProcessed[] = JSON.parse(
    await readFile("./data/addresses.csf.json", {
      encoding: "utf8",
    })
  );
  let captains = 0;
  let officers = 0;
  let crew = 0;
  let passengers = 0;
  for (const elem of file) {
    for (const product of elem.products) {
      if (product == 53) {
        captains++;
      } else if (product === 52) {
        officers++;
      } else if (product === 51) {
        crew++;
      } else if (product === 50) {
        passengers++;
      } else {
        throw new Error(`unknown product ${product}`);
      }
    }
  }

  console.log(`captains ${captains}`);
  console.log(`officers ${officers}`);
  console.log(`crew ${crew}`);
  console.log(`passengers ${passengers}`);
  console.log(`total ${passengers + crew + officers + captains}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
