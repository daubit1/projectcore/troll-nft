import * as dotenv from "dotenv";
dotenv.config({ path: "./.env" });
import { writeFileSync } from "fs";
import { ethers } from "hardhat";
import { shuffle } from "lodash";
import { MongoClient } from "mongodb";
import {
  AddressProcessed,
  CSF_Category,
  DBToken,
  Product,
} from "./types/types";

const { DB_USER, DB_PW, DB_HOST } = process.env;
const MONGODB_URL = `mongodb+srv://${DB_USER}:${DB_PW}@${DB_HOST}/cryptospacefleet?retryWrites=true&w=majority&readPreference=primary&ssl=true`;
let client: MongoClient;
const initTokens = async () => {
  client = await MongoClient.connect(MONGODB_URL);
  const collection = client.db("cryptospacefleet").collection("nfttoken");
  return collection;
};

const initBlacklist = async () => {
  client = await MongoClient.connect(MONGODB_URL);
  const collection = client.db("cryptospacefleet").collection("blacklist");
  return collection;
};

const closeDB = async () => {
  client.close();
};

const toProduct = (category: CSF_Category) => {
  switch (category) {
    case CSF_Category.Passenger:
      return Product.CSFPassenger;
    case CSF_Category.Crew:
      return Product.CSFCrew;
    case CSF_Category.Officer:
      return Product.CSFOfficer;
    case CSF_Category.Captain:
      return Product.CSFCaptain;
  }
};

const blacklistedAddresses = [
  "0xb97770371075AFdCa5925106BCE36E3ff65F993f",
  "0x7F609eb8CEB525b7a0653E887CEba8517766a3E2",
].map((address) => address.toLowerCase());

const rollTokens = (tokens: DBToken[]): DBToken[] => {
  const captains = tokens.filter((x) => x.category === CSF_Category.Captain);
  const officers = tokens.filter((x) => x.category === CSF_Category.Officer);
  const crews = tokens.filter((x) => x.category === CSF_Category.Crew);
  const passangers = tokens.filter(
    (x) => x.category === CSF_Category.Passenger
  );

  console.log(`captains ${captains.length}`);
  console.log(`officers ${officers.length}`);
  console.log(`crews ${crews.length} ${Math.floor(crews.length / 5)}`);
  console.log(
    `passangers ${passangers.length} ${Math.floor(passangers.length / 10)}`
  );
  const shuffledCrews = shuffle(crews).slice(0, Math.floor(crews.length / 5));
  const shuffledPassangers = shuffle(passangers).slice(
    0,
    Math.floor(passangers.length / 10)
  );

  return [
    ...captains,
    ...captains,
    ...officers,
    ...shuffledCrews,
    ...shuffledPassangers,
  ];
};

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const NFT_CONTRACT_ADDRESS = "0xf2901D64eca849f11BE9cF1f8Ba7F2b1EFe30377";

async function main() {
  const CSF = await ethers.getContractFactory("MockCryptospacefleet");
  const csf = CSF.attach(NFT_CONTRACT_ADDRESS);
  const dbBlacklist = await initBlacklist();
  const blacklist = (await dbBlacklist.find({}).toArray()).map(
    (token) => token.tokenId
  );
  console.log("Fetching tokens from db...");
  let dbTokens = (await (await initTokens()).find({}).toArray()) || [];
  dbTokens = dbTokens.filter((token) => token.category !== CSF_Category.Kitty);
  console.log("Fetching token details from chain...");
  const tmp = [];
  for (const token of dbTokens) {
    console.log(token.tokenId);
    const entry = {
      tokenId: token.tokenId,
      category: token.category,
      address: await csf.ownerOf(token.tokenId),
    } as DBToken;
    tmp.push(entry);
    await sleep(500);
  }
  const tokens = tmp.filter(
    (token) =>
      !blacklist.includes(token.tokenId) &&
      !blacklistedAddresses.includes(token.address.toLowerCase())
  );

  console.log("Filtering tokens...");
  const filteredTokens = rollTokens(tokens);

  const processed: { [address: string]: Product[] } = {};
  for (const token of filteredTokens) {
    const product = toProduct(token.category)!;
    if (processed[token.address]) {
      processed[token.address] = [...processed[token.address], product];
    } else {
      processed[token.address] = [product];
    }
  }
  const result: AddressProcessed[] = Object.keys(processed).map((address) => {
    return { address, products: processed[address] };
  });
  await closeDB();
  writeFileSync("data/addresses.csf.json", JSON.stringify(result, null, 1));
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
