/* eslint-disable node/no-missing-import */
// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers, upgrades } from "hardhat";
import { CONTRACT_CID, PROXY_REGISTRY } from "../test/util/const";
import { Storage } from "./storage";

async function main() {
  const chainId = (await ethers.provider.getNetwork()).chainId.toString();
  const storage = new Storage("contracts/address.json");
  let { troll: trollAddress } = storage.fetch(chainId);
  const addresses: any = {};
  // We get the contract to deploy
  if (!trollAddress) {
    const Troll = await ethers.getContractFactory("TrollToken");
    const troll = await upgrades.deployProxy(Troll, [
      PROXY_REGISTRY,
      CONTRACT_CID,
    ]);
    await troll.deployed();
    addresses.troll = troll.address;
    console.log(addresses.troll);
  } else {
    console.log(trollAddress);
  }
  storage.save(chainId, addresses);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
