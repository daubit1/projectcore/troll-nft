import dotenv from "dotenv";
import { ethers } from "hardhat";
dotenv.config();

const TOKEN_CID = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";
const NFT_CONTRACT_ADDRESS = "0x0e1cfB180319058cA1B1A757474112a35662fd35";

async function main() {
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(NFT_CONTRACT_ADDRESS);
  const signer = (await ethers.getSigners())[0];

  let lb = 100;
  let ub = 100;
  let m = 5653;

  const estimateMint = async (amount: number) => {
    const recipients = Array(amount).fill(signer.address);
    const tokenURIs = Array(amount).fill(TOKEN_CID);
    try {
      const gas = await troll.estimateGas.batchMint(recipients, tokenURIs);
      return gas.toNumber();
    } catch (e) {
      return -1;
    }
  };

  const mint = async (amount: number) => {
    const recipients = Array(amount).fill(signer.address);
    const tokenURIs = Array(amount).fill(TOKEN_CID);
    try {
      console.log(`Sending with wallet ${signer.address}`);
      const tx = await troll.batchMint(recipients, tokenURIs, {
        from: signer.address,
      });
      return tx;
    } catch (e) {
      console.log(e);
      return -1;
    }
  };

  const binSearch = async (lb: number, ub: number) => {
    if (lb >= ub) {
      return;
    }
    m = Math.floor((lb + ub) / 2);
    const result = await estimateMint(m);
    if (result === -1) {
      await binSearch(lb, m);
    } else {
      await binSearch(m + 1, ub);
    }
  };

  const upperBound = async (amount: number) => {
    const result = await estimateMint(amount);
    if (result === -1) {
      await binSearch(lb, ub);
    } else {
      lb = ub;
      ub = 2 * lb;
      await upperBound(ub);
    }
  };
  await upperBound(ub);
  console.log(`Approx Array size: ${m}`);
}

main()
  .then(() => process.exit(0))
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
