import dotenv from "dotenv";
import { writeFileSync } from "fs";
import { AddressProcessed, Product } from "./types/types";
import { ethers } from "hardhat";

dotenv.config();
const NFT_CONTRACT_ADDRESS = "0x610de41BDbc0a430f1D48922c8c03D9Ed3D78614";

async function main() {
  const Kartal = await ethers.getContractFactory("KartalCollectible");
  const kartal = Kartal.attach(NFT_CONTRACT_ADDRESS);
  const totalSupply = await kartal.totalSupply();
  console.log(`Total supply: ${totalSupply}`);
  const addressToAmount: { [address: string]: number } = {};
  for (let i = 0; i < totalSupply; i++) {
    const owner = await kartal.ownerOf(i);
    if (addressToAmount[owner]) {
      addressToAmount[owner] = addressToAmount[owner] + 1;
    } else {
      addressToAmount[owner] = 1;
    }
  }
  const result: AddressProcessed[] = Object.keys(addressToAmount).map(
    (address) => {
      return {
        address,
        products: Array(addressToAmount[address]).fill(Product.KartalNFT),
      };
    }
  );
  writeFileSync("data/kartal.json", JSON.stringify(result, null, 2));
}

main().then(() => process.exit(0));
