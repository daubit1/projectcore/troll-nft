import * as dotenv from "dotenv";
dotenv.config({ path: "./.env" });
import { getName, getAttribsFromFilename } from "../util/metadata";
import { readFile } from "fs/promises";
import { uploadMetadataToIPFS } from "../util/upload";
import { writeFileSync } from "fs";

let lookup: Record<string, unknown> = {};
let timestamp: string | number = Date.now();

const NFT_TYPE = "kartal";

if (process.argv.length < 3) {
  console.log(
    "missing parameter. example: node preupload-metadata.js <art-lookup-identifier> [metadata-lookup-identifier]"
  );
  process.exit(1);
}

function shuffle<T>(array: T[]) {
  array.sort(() => Math.random() - 0.5);
}

async function preuploadMetadata() {
  const metadataTemplate = JSON.parse(
    await readFile("./data/metadatatemplate.json", { encoding: "utf-8" })
  );
  const imageIpfsLookup = JSON.parse(
    await readFile(`./data/cids/image-ipfs-lookup-${process.argv[2]}.json`, {
      encoding: "utf-8",
    })
  );
  const indivFilesExcludes = JSON.parse(
    await readFile("./data/indiv-exclude.json", { encoding: "utf-8" })
  );
  if (indivFilesExcludes.length >= 1 && indivFilesExcludes[0].endsWith("jpg")) {
    throw new Error("indivFilesExcludes does not end in jpg");
  }

  const lookuptableEntries = Object.entries(imageIpfsLookup).filter(
    (x) => !indivFilesExcludes.includes(x)
  );
  shuffle(lookuptableEntries);
  let i = 1;

  const metadataArray = [];
  for (const artlookup of lookuptableEntries) {
    const metadataAttribs = getAttribsFromFilename(NFT_TYPE, artlookup[0]);
    const metadata = {
      ...metadataTemplate,
      image: `ipfs://${artlookup[1]}`,
      attributes: metadataAttribs,
      name: getName(NFT_TYPE, metadataAttribs, metadataTemplate),
    };
    i++;
    metadataArray.push({ data: metadata, filename: artlookup[0] });
  }

  console.log(metadataArray.length);
  for (let i = 0; i < metadataArray.length; i++) {
    const metadata = metadataArray[i];
    try {
      const ipfsMetaDataUploadResult = await uploadMetadataToIPFS(
        metadata.data
      );

      if (ipfsMetaDataUploadResult.IpfsHash) {
        lookup[metadata.filename] = ipfsMetaDataUploadResult.IpfsHash;
        console.log(
          `${i}/${metadataArray.length} Uploaded metadata from ${metadata.filename} to ipfs hash: ${ipfsMetaDataUploadResult.IpfsHash}`
        );
      } else {
        break;
      }
    } catch (err) {
      writeFileSync(
        `./data/cids/metadata-ipfs-lookup-${timestamp}.json`,
        JSON.stringify(lookup)
      );
      console.log(
        `wrote cids to ./data/cids/metadata-ipfs-lookup-${timestamp}.json ${
          Object.keys(lookup).length
        }`
      );
    }
  }
}

if (process.argv.length >= 4) {
  timestamp = process.argv[3];
}

preuploadMetadata()
  .then(() => {
    writeFileSync(
      `./data/cids/metadata-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/metadata-ipfs-lookup-${timestamp}.json ${
        Object.keys(lookup).length
      }`
    );
  })
  .catch((err) => {
    console.log(err);
    writeFileSync(
      `./data/cids/metadata-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/metadata-ipfs-lookup-${timestamp}.json`
    );
    process.exit(1);
  });
