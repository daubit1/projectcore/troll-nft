const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 3) {
  console.log(
    "missing parameter. example: node mint-from-preupload.js <batch-identifier> <batch>"
  );
  process.exit(1);
}

require("dotenv").config();

const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;
const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const NETWORK = process.env.NETWORK;
const CHUNCK_SIZE = process.env.CHUNCK_SIZE;

if (!CHUNCK_SIZE) {
  throw new Error("invalid CHUNCK_SIZE");
}

import { readFile } from "fs/promises";
import { writeFileSync } from "fs";
import { chunk } from "lodash";
import { AddressBatch } from "../types/types";
import { ethers } from "hardhat";

const usedFilenames: string[] = [];

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function mint(batchIdentifier: string, batch: string) {
  const entries: AddressBatch[] = JSON.parse(
    await readFile(`./data/addressBatch/${batchIdentifier}/${batch}.json`, {
      encoding: "utf-8",
    })
  );

  const gasPrice = await ethers.provider.getGasPrice();
  console.log(`gasprice: ${ethers.utils.formatUnits(gasPrice, "gwei")} gwei`);
  console.log(`gasprice: ${gasPrice} wei`);
  //const gasPrice2 = gasPrice + "000";
  //console.log(`gasprice: ${web3.utils.fromWei(gasPrice2, "gwei")} gwei`);
  console.log(`gasprice: ${ethers.utils.formatUnits("100", "gwei")} wei`);
  //console.log(`gasprice: ${gasPrice2} wei`);
  const Contract = await ethers.getContractFactory("TrollToken");
  const nftContract = Contract.attach(NFT_CONTRACT_ADDRESS!);

  console.log(`contract: ${NFT_CONTRACT_ADDRESS}`);
  console.log(`network: ${NETWORK}`);
  console.log(`provider: ${ethers.provider}`);

  await sleep(4000);

  const batches = chunk(entries, parseInt(CHUNCK_SIZE!));

  for (const data of batches) {
    try {
      console.log(
        `start minting token to ${data
          .map((x) => x.address)
          .join(",")} with metadata ${data.map((x) => x.metadata).join(",")}`
      );
      console.log(data);

      const mintResult = await nftContract
        .batchMint(
          data.map((x) => x.address),
          data.map((x) => x.metadata)
        )
        .send({ from: OWNER_ADDRESS });

      console.log(
        `minted tokens to ${data.map((x) => x.address).join(",")} tx: ${
          mintResult.transactionHash
        }`
      );
      usedFilenames.push(...data.map((x) => x.filename));
    } catch (err) {
      writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
      throw err;
    }
  }
}

mint(process.argv[nodeIndex + 2], process.argv[nodeIndex + 3])
  .then(() => {
    writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
    process.exit(0);
  })
  .catch((err) => {
    console.log(err);
    writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
    process.exit(1);
  });
