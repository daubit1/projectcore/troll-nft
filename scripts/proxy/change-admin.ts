require("dotenv").config();
import { ethers } from "hardhat";

async function main() {
  const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
  const Contract = await ethers.getContractFactory("TrollToken");
  const nftContract = Contract.attach(
    "0x10acaa621Cc3955804D21e97d1B639305dbD7B23"
  );

  nftContract.methods
    .changeAdmin("0x1aDce6bcc3f5127c760481E0EF270307DeD604A6")
    .send({ from: OWNER_ADDRESS })
    .then(() => console.log("done"))
    .catch((err: any) => console.log(err));
}

main().then(() => process.exit(0));
