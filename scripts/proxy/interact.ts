require("dotenv").config();
import { ethers } from "hardhat";

const CONTRACT_ADDRESS = "0xAb60e8d6b8F6EE644573236D147a77F9Ac29A50e";
const MINTER_ROLE =
  "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6";

async function main() {
  const Contract = await ethers.getContractFactory("TrollToken");
  const contract = Contract.attach(CONTRACT_ADDRESS);

  contract
    .hasRole(MINTER_ROLE, "0xfa425cfd82a04d1c992a5a9417ea49b05b453ca0")
    .then((result: any) => console.log(result))
    .catch((err: any) => console.log(err));

  contract
    .hasRole("0x00", "0xfa425cfd82a04d1c992a5a9417ea49b05b453ca0")
    .then((result: any) => console.log(result))
    .catch((err: any) => console.log(err));

  contract
    .grantRole(MINTER_ROLE, "0xfa425cfd82a04d1c992a5a9417ea49b05b453ca0")
    .then((result: any) => console.log(result))
    .catch((err: any) => console.log(err));
}

main().then(() => process.exit(0));
