import { readFile, writeFile } from "fs/promises";

async function main() {
  const file = JSON.parse(
    await readFile("./data/cids/metadata-ipfs-lookup-kartal.json", {
      encoding: "utf8",
    })
  );
  const entries = Object.entries(file);
  const _tattoos = entries.filter(([k, _]) => k.includes("SKTA"));
  const _noTattoos = entries.filter(([k, _]) => !k.includes("SKTA"));

  const tattoos: Record<string, unknown> = {};
  for (const [k, v] of _tattoos) {
    tattoos[k] = v;
  }
  const noTattoos: Record<string, unknown> = {};
  for (const [k, v] of _noTattoos) {
    noTattoos[k] = v;
  }

  await writeFile(
    "./data/cids/metadata-ipfs-lookup-tattoo.json",
    JSON.stringify(tattoos)
  );
  await writeFile(
    "./data/cids/metadata-ipfs-lookup-notattoo.json",
    JSON.stringify(noTattoos)
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
