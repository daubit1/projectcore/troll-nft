import { formatEther, formatUnits, parseEther } from "ethers/lib/utils";
import { readFileSync } from "fs";
import { readFile } from "fs/promises";
import { HardhatRuntimeEnvironment } from "hardhat/types";
import { AddressBatch } from "./types/types";

interface TrollContractArgs {
  address: string;
  id: number;
}

interface AddCollectionArgs extends TrollContractArgs {
  name: string;
  ws: number;
  fs: number;
  a: number;
}

interface BatchMintArgs extends TrollContractArgs {
  batchid: string;
  batch: string;
}

interface SetContractURIArgs extends TrollContractArgs {
  cid: string;
}

interface AddURIsArgs extends TrollContractArgs {
  path: string;
}

interface DonateArgs {
  address: string;
}

export async function addCollection(
  args: AddCollectionArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, name, ws: wlStart, fs: freeStart, a: amount } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const addTx = await troll.addCollection(name, wlStart, freeStart, amount);
    await addTx.wait();
    console.log(`Collection ${name} has been added to ${address}`);
  } catch (e) {
    console.log(e);
  }
}

export async function setContractURI(
  args: SetContractURIArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, cid } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const updateTx = await troll.setContractURI(cid);
    await updateTx.wait();
    console.log(`updated contract metadata`);
  } catch (e) {
    console.log(e);
  }
}

export async function batchMint(
  args: BatchMintArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, batch, batchid } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const entries: AddressBatch[] = JSON.parse(
      await readFile(`./data/addressBatch/${batchid}/${batch}.json`, {
        encoding: "utf-8",
      })
    );

    const addresses = entries.map((x) => x.address);
    const metadata = entries.map((x) => x.metadata);

    const mintTx = await troll.batchMint(addresses, metadata);
    await mintTx.wait();
    console.log(`Minted ${`./data/addressBatch/${batchid}/${batch}.json`}`);
  } catch (e) {
    console.log(e);
  }
}

export async function getCollection(
  args: TrollContractArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, id } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const name = await troll.getCollection(id);
    console.log(`Collection name of id ${id} is ${name}`);
  } catch (e) {
    console.log(e);
  }
}

export async function addURIs(
  args: AddURIsArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, path, id } = args;
  const uris = JSON.parse(readFileSync(path, "utf8"));
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  const code = await ethers.provider.getCode(address);

  try {
    if (code === "0x") {
      throw new Error(`No code at contract ${address}`);
    }
    const addURIsTx = await troll.addURIs(
      id,
      // @ts-ignore
      uris.map((x) => x.metadata)
    );
    await addURIsTx.wait();
    console.log("URIs successfully added!");
  } catch (e) {
    console.log(e);
  }
}

interface Whitelist {
  addresses: string[];
  amounts: number[];
}

export async function addToWhitelist(
  args: AddURIsArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, path, id } = args;
  const whitelist = JSON.parse(readFileSync(path, "utf8")) as Whitelist;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  const code = await ethers.provider.getCode(address);
  try {
    if (code === "0x") {
      throw new Error(`No code at contract ${address}`);
    }
    const addWLTx = await troll.addToWhitelist(
      id,
      whitelist.addresses,
      whitelist.amounts
    );
    await addWLTx.wait();
    console.log("Whitelist successfully added!");
  } catch (e) {
    console.log(e);
  }
}

export async function totalSupply(
  args: TrollContractArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, id } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const name = await troll.getCollection(id);
    const totalSupply = await troll["totalSupply(uint256)"](id);
    console.log(`Collection ${name} has ${totalSupply} URIs!`);
  } catch (e) {
    console.log(e);
  }
}

export async function donate(args: DonateArgs, hre: HardhatRuntimeEnvironment) {
  const { ethers } = hre;
  const { address } = args;
  const signers = await ethers.getSigners();
  const donater = signers[signers.length - 1];
  try {
    const value = parseEther("5");
    const sendTx = await donater.sendTransaction({ to: address, value: value });
    await sendTx.wait();
    console.log(`${donater.address} sent ${formatEther(value)} to ${address}`);
  } catch (e) {
    console.log(e);
  }
}

interface SaleArgs extends TrollContractArgs {
  t: number;
}

export async function setPrivateSale(
  args: SaleArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, t, id } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const setTx = await troll["setWhitelistMintStart(uint256,uint256)"](id, t);
    await setTx.wait();
    console.log(`Time succefully set to ${new Date(t * 1000)}`);
  } catch (e) {
    console.log(e);
  }
}

export async function setPublicSale(
  args: SaleArgs,
  hre: HardhatRuntimeEnvironment
) {
  const { ethers } = hre;
  const { address, t, id } = args;
  const Troll = await ethers.getContractFactory("TrollToken");
  const troll = Troll.attach(address);
  try {
    const setTx = await troll["setFreeMintStart(uint256,uint256)"](id, t);
    await setTx.wait();
    console.log(`Time succefully set to ${new Date(t * 1000)}`);
  } catch (e) {
    console.log(e);
  }
}
