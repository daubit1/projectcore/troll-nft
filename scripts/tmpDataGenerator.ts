import { keccak256 } from "ethers/lib/utils";
import { readFileSync, writeFileSync } from "fs";
import { ethers } from "hardhat";
import { AddressProcessed } from "./types/types";

async function main() {
  const addresses: AddressProcessed[] = [];
  for (let i = 0; i < 5000 - 672; i++) {
    addresses.push({
      address: "0xFa425cFd82A04D1C992A5a9417Ea49B05b453ca0",
      products: [50],
    });
  }
  writeFileSync("./data/test.addresses.json", JSON.stringify(addresses));

  const imageIpfsLookup = JSON.parse(
    readFileSync(`./data/cids/image-ipfs-lookup-1657022221537.json`, {
      encoding: "utf-8",
    })
  );
  const testMetadata: Record<string, string> = {};
  for (const e of Object.entries(imageIpfsLookup)) {
    testMetadata[e[0]] = "Qmb9aKQJA2GU79xuCLFp7NFimk8y9cAo32janmZXUkrqm1";
  }
  writeFileSync(
    "./data/cids/metadata-ipfs-lookup-test.json",
    JSON.stringify(testMetadata)
  );

  console.log(keccak256("0x10"));
  const whitelist = {
    addresses: Array.from(
      { length: 89 },
      (_, x) => ethers.Wallet.createRandom().address
    ),
    amounts: Array(89).fill(1),
  };
  writeFileSync("./data/whitelist-test.json", JSON.stringify(whitelist));
}

main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
