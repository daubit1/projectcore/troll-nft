export interface Metadata {
  data: {
    name: string;
    description: string;
    image: string;
    attributes: { trait_type: string; value: string }[];
  };
  filename: string;
}

export interface AddressProcessed {
  address: string;
  products: Product[];
  order_id?: number[];
  email?: string;
}

export interface AddressBatch {
  raw: AddressProcessed;
  address: string;
  metadata: string;
  filename: string;
  product: Product;
}

export enum CSF_Category {
  Kitty,
  Passenger,
  Crew,
  Officer,
  Captain,
}

export interface DBToken {
  category: CSF_Category;
  address: string;
  tokenId: number;
}

export enum Product {
  KartalNFT = 0,
  CSFPassenger = 50,
  CSFCrew = 51,
  CSFOfficer = 52,
  CSFCaptain = 53,
  CSFTest = 54,
  CSFTest2 = 55,
}
