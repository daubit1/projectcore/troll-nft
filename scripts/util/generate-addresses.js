const { writeFile } = require("fs/promises");

async function work() {
  const template = {
    address: "0xFa425cFd82A04D1C992A5a9417Ea49B05b453ca0",
    products: [53, 53],
  };
  const arr = [];
  for (let i = 0; i < 200; i++) {
    arr.push({ ...template });
  }
  await writeFile("./data/addresses.processed.2.json", JSON.stringify(arr));
}

work()
  .then(() => process.exit(0))
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
