const { readFile, writeFile } = require("fs/promises");

const colors = [
  "BB",
  "WW",
  "YE",
  "BL",
  "RE",
  "GR",
  "VI",
  "OR",
  "BR",
  "GY",
  "BU",
];

const furs = ["FIC", "FTI", "FLE", "FPA", "FSK"];
const podests = ["PGB", "PGM", "PGD", "PGV"];
const eyes = ["EYE", "EBB", "EBD", "ERE", "EGR", "EOR", "EVI"];

async function work() {
  const artTemplate = await readFile("./data/art.png");

  for (let i = 0; i < 500; i++) {
    const filename = `./data/art/CSF-${
      furs[Math.floor(Math.random() * furs.length)]
    }-${colors[Math.floor(Math.random() * colors.length)]}-${
      colors[Math.floor(Math.random() * colors.length)]
    }-${podests[Math.floor(Math.random() * podests.length)]}-${
      eyes[Math.floor(Math.random() * eyes.length)]
    }.png`;
    await writeFile(filename, artTemplate);
  }
}

work()
  .then(() => process.exit(0))
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
