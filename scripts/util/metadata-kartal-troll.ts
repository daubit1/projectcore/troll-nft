function getClothingColor(color: string) {
  let value = "";
  switch (color) {
    case "CLGY":
      value = "Grey";
      break;
    case "CLVI":
      value = "Violett";
      break;
    case "CLPI":
      value = "Pink";
      break;
    case "CLWH":
      value = "Weiss";
      break;
    default:
      throw new Error(`invalid clothingcolor type ${color}`);
  }
  return { trait_type: "Clothing Color", value: value };
}

function getFeatherColor(color: string) {
  let value = "";
  switch (color) {
    case "HBWH":
      value = "White";
      break;
    case "HBGY":
      value = "Grey";
      break;
    case "HBBR":
      value = "Brown";
      break;
    case "HBYE":
      value = "Blond";
      break;
    case "HBPI":
      value = "Pink";
      break;
    case "HFGY":
      value = "Grey";
      break;
    case "HFWH":
      value = "White";
      break;
    case "HFBR":
      value = "Brown";
      break;
    case "HFYE":
      value = "Yellow";
      break;
    case "HFPI":
      value = "Pink";
      break;

    default:
      throw new Error(`invalid clothingcolor type ${color}`);
  }
  return { trait_type: "Feather Color", value: value };
}

function getHairStyle(color: string) {
  let value = "";
  switch (color) {
    case "HBWH":
      value = "Bald";
      break;
    case "HBGY":
      value = "Bald";
      break;
    case "HBBR":
      value = "Bald";
      break;
    case "HBYE":
      value = "Bald";
      break;
    case "HBPI":
      value = "Bald";
      break;
    case "HFGY":
      value = "Full";
      break;
    case "HFWH":
      value = "Full";
      break;
    case "HFBR":
      value = "Full";
      break;
    case "HFYE":
      value = "Full";
      break;
    case "HFPI":
      value = "Full";
      break;

    default:
      throw new Error(`invalid clothingcolor type ${color}`);
  }
  return { trait_type: "Hair Style", value: value };
}

function getWingPatternColor(color: string) {
  let value = "";
  switch (color) {
    case "WIBL":
      value = "Blue";
      break;
    case "WIGR":
      value = "Green";
      break;
    case "WIRE":
      value = "Red";
      break;
    case "WIGY":
      value = "Grey";
      break;
    default:
      throw new Error(`invalid wingpatterncolor type ${color}`);
  }
  return { trait_type: "Wing Pattern Color", value: value };
}

function getBackgroundColor(color: string) {
  let value = "";
  switch (color) {
    case "BGPI":
      value = "Pink";
      break;
    case "BGTU":
      value = "Turquoise";
      break;
    case "BGRE":
      value = "Red";
      break;
    case "BGBL":
      value = "Blue";
      break;
    case "BGBW":
      // #TODO: what
      value = "Black";
      break;
    default:
      throw new Error(`invalid backgroundcolor type ${color}`);
  }
  return { trait_type: "Background Color", value: value };
}

function getSkinColor(color: string) {
  let value = "";
  switch (color) {
    case "SKBR":
      value = "Bright";
      break;
    case "SKDA":
      value = "Dark";
      break;
    case "SKTA":
      value = "Tattoo";
      break;
    default:
      throw new Error(`invalid skincolor type ${color}`);
  }
  return { trait_type: "Skin Color", value: value };
}

function getEyeColor(color: string) {
  let value = "";
  switch (color) {
    case "EYYE":
      value = "Yellow";
      break;
    case "EYPI":
      value = "Pink";
      break;
    case "EYGR":
      value = "Green";
      break;
    case "EYBL":
      value = "Blue";
      break;
    case "EYWH":
      value = "White";
      break;
    default:
      throw new Error(`invalid eye color type ${color}`);
  }
  return { trait_type: "Eye Color", value: value };
}

export function getKartalTrollAttribs(filename: string) {
  const filenameWoExt = filename.split(".")[0];
  const splitFilenames = filenameWoExt.split("-");
  if (splitFilenames.length !== 6) {
    throw new Error(
      `filename ${filename} does not match kartal troll scheme, length ${splitFilenames.length}`
    );
  }
  const attribs: { trait_type: string; value: string }[] = [];
  attribs.push({ trait_type: "Category", value: "Kärtäl" });
  attribs.push(getClothingColor(splitFilenames[0]));
  attribs.push(getFeatherColor(splitFilenames[1]));
  attribs.push(getHairStyle(splitFilenames[1]));
  attribs.push(getWingPatternColor(splitFilenames[2]));
  attribs.push(getBackgroundColor(splitFilenames[3]));
  attribs.push(getSkinColor(splitFilenames[4]));
  attribs.push(getEyeColor(splitFilenames[5]));
  return attribs;
}
