import { getKartalTrollAttribs } from "./metadata-kartal-troll";

export function getAttribsFromFilename(type: string, filename: string) {
  if (type === "kartal") {
    return getKartalTrollAttribs(filename);
  } else {
    console.log(`invalid type ${type}`);
    process.exit(1);
  }
}

export function getName(
  category: string,
  metadataAttribs: { value: string }[],
  metadataTemplate: Record<string, unknown>
) {
  if (category === "kartal") {
    return `${metadataAttribs[3].value}-${
      metadataAttribs[3].value === "Bald" ? "Head" : "Hair"
    }-${metadataAttribs[4].value}-Wings-${metadataTemplate.name}`;
  } else {
    throw new Error(`unknown category ${category}`);
  }
}
