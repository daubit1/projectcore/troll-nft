import { rename, readdir } from "fs/promises";

async function work() {
  const files = (await readdir("./data/art")).filter((x) => x !== ".gitkeep");

  for (const file of files) {
    const newFilename = `./data/art/CSF-${file
      .substring(file.length - 17 - 4)
      .toUpperCase()}`;
    await rename(`./data/art/${file}`, newFilename);
  }
}

work()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
