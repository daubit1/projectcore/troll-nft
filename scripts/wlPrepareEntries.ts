import csv from "csv-parser";
import { createReadStream, writeFileSync } from "fs";

function main() {
  const results: any[] = [];
  createReadStream("./data/entries.csv")
    .pipe(csv())
    .on("data", (data: any) => results.push(data))
    .on("end", () => {
      writeFileSync(
        "./data/whitelist.json",
        JSON.stringify({
          addresses: results.map((x) => x.wallet),
          amounts: results.map((_) => 1),
        })
      );
    });
}

main();
