import {
  CONTRACT_CID,
  ADMIN_ROLE,
  MINTER_ROLE,
  CONTRACT_CID_V2,
  CONTRACT_URI,
  CONTRACT_URI_V2,
  TOKEN_CID,
  PROXY_REGISTRY,
} from "./util/const";
import { expect } from "chai";
import { Contract } from "ethers";
import { ethers, upgrades } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("TrollToken", async () => {
  let troll: Contract;
  let mockTroll: Contract;
  let mockProxyRegistry: Contract;
  let NEW_PROXY_REGISTRY_ADDRESS: string;
  let owner: SignerWithAddress; //Owner of the troll contract
  let admin: SignerWithAddress; //Admin of the proxy contract,
  let userA: SignerWithAddress;
  let proxyForOwner: SignerWithAddress;
  before(async () => {
    const accounts = await ethers.getSigners();
    owner = accounts[0]; //Owner of the troll contract
    admin = accounts[1]; //Admin of the proxy contract,
    userA = accounts[2];
    proxyForOwner = accounts[8];
    const MockProxyRegistry = await ethers.getContractFactory(
      "MockProxyRegistry"
    );
    mockProxyRegistry = await MockProxyRegistry.deploy();
    await mockProxyRegistry.setProxy(owner.address, proxyForOwner.address);
    NEW_PROXY_REGISTRY_ADDRESS = mockProxyRegistry.address;
    const TrollToken = await ethers.getContractFactory("TrollToken");
    troll = await upgrades.deployProxy(TrollToken, [
      PROXY_REGISTRY,
      CONTRACT_CID,
    ]);
  });
  describe("Interacting with V1 contract", async () => {
    describe("Proxy", async () => {
      describe("#grantRole()", async () => {
        it("can grant admin ownership", async () => {
          expect(troll.grantRole(ADMIN_ROLE, admin.address)).to.not.be.reverted;
        });
      });
      describe("#grantRole()", async () => {
        it("should error when send from non-admin", async () => {
          expect(troll.connect(userA).grantRole(ADMIN_ROLE, userA.address)).to
            .be.reverted;
        });
      });
    });
    describe("Roles", () => {
      it("Owner is admin", async () => {
        expect(await troll.hasRole(ADMIN_ROLE, owner.address)).to.be.true;
      });
      it("Owner is minter", async () => {
        expect(await troll.hasRole(MINTER_ROLE, owner.address)).to.be.true;
      });
      it("Admin is admin", async () => {
        expect(await troll.hasRole(ADMIN_ROLE, admin.address)).to.be.true;
      });
      it("Admin is NOT minter", async () => {
        expect(await troll.hasRole(MINTER_ROLE, admin.address)).to.be.false;
      });
      it("User is NOT admin", async () => {
        expect(await troll.hasRole(ADMIN_ROLE, userA.address)).to.be.false;
      });
      it("User is NOT minter", async () => {
        expect(await troll.hasRole(MINTER_ROLE, userA.address)).to.be.false;
      });
    });

    describe("TrollToken", () => {
      describe("#constructor()", async () => {
        it("should set the contractURI to the supplied value", async () => {
          expect(await troll.contractURI()).to.equal(CONTRACT_URI);
        });
      });
      describe("#name()", async () => {
        it("should return the correct name", async () => {
          expect(await troll.name()).to.equal("TrollToken");
        });
      });
      describe("#baseTokenURI()", async () => {
        it("should return ipfs://", async () => {
          expect(await troll.baseTokenURI()).to.equal("ipfs://");
        });
      });
      describe("#symbol()", async () => {
        it("should return the correct symbol", async () => {
          expect(await troll.symbol()).to.equal("TT");
        });
      });
      describe("#contractURI()", async () => {
        it("should return the correct URI", async () => {
          expect(await troll.contractURI()).to.equal(CONTRACT_URI);
        });
      });
      describe("#setContractURI()", async () => {
        it("should set the contractURI only from admin", async () => {
          await troll.setContractURI(CONTRACT_CID_V2);
          expect(await troll.contractURI()).to.equal(CONTRACT_URI_V2);
        });
        it("should not set the contractURI from non-admins", async () => {
          expect(troll.connect(userA).setContractURI(CONTRACT_CID_V2)).to.be
            .reverted;
        });
      });
      describe("#setProxyAddress()", async () => {
        it("should revert when setting same proxy address", async () => {
          expect(troll.connect(owner).setProxyAddress(PROXY_REGISTRY)).to.be
            .reverted;
        });
        it("should revert when set from non-owner", async () => {
          expect(troll.connect(userA).setProxyAddress(PROXY_REGISTRY)).to.be
            .reverted;
        });
        it("should set new proxy address when called from owner", async () => {
          expect(
            troll.connect(owner).setProxyAddress(NEW_PROXY_REGISTRY_ADDRESS)
          ).to.not.be.reverted;
        });
      });
      describe("#isApprovedForAll()", async () => {
        it("should return false when approving for owner", async () => {
          expect(
            await troll
              .connect(owner)
              .isApprovedForAll(userA.address, owner.address)
          ).to.be.false;
        });
        it("should approve when called with proxy registry", async () => {
          expect(
            await troll
              .connect(owner)
              .isApprovedForAll(userA.address, NEW_PROXY_REGISTRY_ADDRESS)
          ).to.be.true;
        });
      });
    });
    describe("Collection", async () => {
      describe("#addCollection()", async () => {
        it("should not be able to mint without a collection", async () => {
          expect(
            troll.connect(userA).mint(1, userA.address)
          ).to.be.revertedWith("Invalid collection id!");
        });
        it("should allow admin to add a new collection", async () => {
          expect(troll.connect(admin).addCollection("Kartal", 4, 5, 2)).to.not
            .be.reverted;
        });
        it("should not allow user to add a new collection", async () => {
          expect(troll.connect(admin).addCollection("Wurstfinger", 4, 3, 2)).to
            .be.reverted;
        });
        it("should return correct collection name", async () => {
          expect(await troll.connect(admin).getCollection(1)).to.be.equal(
            "Kartal"
          );
          expect(await troll.connect(admin).getFreeMintStart(1)).to.be.equal(5);
          expect(
            await troll.connect(admin).getWhitelistMintStart(1)
          ).to.be.equal(4);
        });
        it("getFreeMintStart and setFreeMintStart reverts for invalid collections", async () => {
          expect(
            troll.connect(owner).getFreeMintStart(3468)
          ).to.be.revertedWith("Error: Invalid collection id!");
          expect(
            troll.connect(owner).setFreeMintStart(345434, 34)
          ).to.be.revertedWith("Error: Invalid collection id!");
        });
      });
    });
    describe("Mint Limit", () => {
      describe("#getMintLimit()", () => {
        it("should return correct limit", async () => {
          expect(await troll.getFreeMintLimit(1)).to.be.equal(2);
        });
      });
      describe("#setFreeMintLimit()", () => {
        it("should allow admins to set a new mint limit", async () => {
          const setTx = await troll.connect(owner).setFreeMintLimit(1, 1);
          await setTx.wait();
          expect(await troll.getFreeMintLimit(1)).to.be.equal(1);
          const setTx2 = await troll.connect(owner).setFreeMintLimit(1, 2);
          await setTx2.wait();
          expect(await troll.getFreeMintLimit(1)).to.be.equal(2);
        });
        it("should not allow users to set a new mint limit", async () => {
          expect(troll.connect(userA).setFreeMintLimit(1)).to.be.reverted;
        });
      });
    });
    describe("URI", () => {
      describe("#mint()", () => {
        it("should not be able to mint without URIs", async () => {
          expect(
            troll.connect(userA).mint(1, userA.address)
          ).to.be.revertedWith("Error: Out of bounds!");
        });
      });
      describe("#addURI()", () => {
        it("should allow admin to add URI", async () => {
          const addURI = await troll.connect(owner).addURI(1, TOKEN_CID);
          await addURI.wait();
          expect(await troll["totalSupply(uint256)"](1)).to.be.equal(1);
        });
        it("should not allow user to send a URI", async () => {
          expect(troll.connect(userA).addURI(1, TOKEN_CID)).to.be.reverted;
        });
        it("should not allow to send a empty URI", async () => {
          expect(troll.connect(owner).addURI(1, "")).to.be.reverted;
        });
        it("should allow admin to send URIs", async () => {
          const addURIs = await troll
            .connect(owner)
            .addURIs(1, Array(10).fill(TOKEN_CID));
          await addURIs.wait();
          expect(await troll["totalSupply(uint256)"](1)).to.be.equal(11);
        });
        it("should not allow to send empty URIs", async () => {
          expect(troll.connect(owner).addURIs(1, [])).to.be.reverted;
        });
        it("should not allow users to send URIs", async () => {
          expect(troll.connect(userA).addURIs(1, Array(10).fill(TOKEN_CID))).to
            .be.reverted;
        });
      });
      describe("#removeURI()", () => {
        it("should allow admin to remove an URI", async () => {
          const remove = await troll.removeURI(1, 0);
          await remove.wait();
          expect(await troll["totalSupply(uint256)"](1)).to.be.equal(10);
        });
        it("should not allow users to remove an URI", async () => {
          expect(troll.connect(userA).removeURI(1, 0)).to.be.reverted;
        });
      });
    });
    describe("Minting", () => {
      describe("#mint()", () => {
        it("should allow owner to mint", async () => {
          const mint = await troll.connect(owner).mint(1, owner.address);
          await mint.wait();
          expect(await troll.ownerOf(1)).to.be.equal(owner.address);
        });
        it("should allow user to mint", async () => {
          const mint = await troll.connect(userA).mint(1, userA.address);
          await mint.wait();
          expect(await troll.ownerOf(2)).to.be.equal(userA.address);
        });
        it("should not allow any to mint more than two times", async () => {
          const mint = await troll.connect(userA).mint(1, userA.address);
          await mint.wait();
          expect(troll.connect(userA).mint(1, userA.address)).to.be.reverted;
          expect(await troll.ownerOf(3)).to.be.equal(userA.address);
        });
        it("should revert von invalid collections", async () => {
          expect(
            troll.connect(userA).whitelistMint(4, userA.address)
          ).to.be.revertedWith("Error: Invalid collection id!");
        });
        it("should not allow to mint before freeMintStartTime", async () => {
          const date = new Date();
          date.setDate(date.getDate() + 1);
          const ts = Math.floor(date.getTime() / 1000);
          let setStartTime = await troll.connect(owner).setFreeMintStart(1, ts);
          await setStartTime.wait();
          expect(await troll.getFreeMintStart(1)).to.be.equal(ts);
          await expect(
            troll.connect(userA).mint(1, userA.address)
          ).to.be.revertedWith("Error: free mint has not started yet");
          setStartTime = await troll.connect(owner).setFreeMintStart(1, 0);
          await setStartTime.wait();
        });
      });
      describe("#whitelistMint()", () => {
        it("should allow adding to whitelist for owner", async () => {
          const addTx = await troll
            .connect(owner)
            .addToWhitelist(1, [userA.address, owner.address], [2, 3]);
          await addTx.wait();
          expect(
            await troll.connect(owner).getWhitelistSlots(1, userA.address)
          ).to.equal(2);
          expect(
            await troll.connect(userA).getWhitelistSlots(1, owner.address)
          ).to.equal(3);
        });
        it("addToWhitelist reverts for array of unequal length", async () => {
          expect(
            troll.connect(owner).addToWhitelist(1, [userA.address], [2, 3])
          ).to.revertedWith("Error: Array lengths mismatch");
        });
        it("getWhitelistSlots reverts for invalid collections", async () => {
          expect(
            troll.connect(owner).getWhitelistSlots(99, userA.address)
          ).to.be.revertedWith("Error: Invalid collection id!");
        });
        it("should not allow adding to whitelist for users", async () => {
          expect(troll.connect(userA).addToWhitelist(1, [userA.address], [2]))
            .to.be.reverted;
        });
        it("should allow owner to whitelistMint", async () => {
          const whitelistMint = await troll
            .connect(owner)
            .whitelistMint(1, owner.address);
          await whitelistMint.wait();
          expect(await troll.ownerOf(1)).to.be.equal(owner.address);
        });
        it("should allow user to whitelistMint", async () => {
          const whitelistMint = await troll
            .connect(userA)
            .whitelistMint(1, userA.address);
          await whitelistMint.wait();
          expect(await troll.ownerOf(2)).to.be.equal(userA.address);
        });
        it("should not allow any to whitelistMint more than two times", async () => {
          const whitelistMint = await troll
            .connect(userA)
            .whitelistMint(1, userA.address);
          await whitelistMint.wait();
          expect(troll.connect(userA).whitelistMint(1, userA.address)).to.be
            .reverted;
          expect(await troll.ownerOf(3)).to.be.equal(userA.address);
        });
        it("should revert von invalid collections", async () => {
          expect(
            troll.connect(userA).whitelistMint(4, userA.address)
          ).to.be.revertedWith("Error: Invalid collection id!");
        });
        it("should not allow to mint before whitelistMintStartTime", async () => {
          const date = new Date();
          date.setDate(date.getDate() + 1);
          const ts = Math.floor(date.getTime() / 1000);
          let setStartTime = await troll
            .connect(owner)
            .setWhitelistMintStart(1, ts);
          await setStartTime.wait();
          expect(await troll.getWhitelistMintStart(1)).to.be.equal(ts);
          await expect(
            troll.connect(userA).whitelistMint(1, userA.address)
          ).to.be.revertedWith("Error: whitelist mint has not started yet");
          setStartTime = await troll.connect(owner).setWhitelistMintStart(1, 0);
          await setStartTime.wait();
        });
        it("getWhitelistMintStart and setWhitelistMintStart reverts for invalid collections", async () => {
          expect(
            troll.connect(owner).getWhitelistMintStart(42000)
          ).to.be.revertedWith("Error: Invalid collection id!");
          expect(
            troll.connect(owner).setWhitelistMintStart(765, 45)
          ).to.be.revertedWith("Error: Invalid collection id!");
        });
      });
      describe("#burn()", () => {
        it("should not allow the non-owner to burn the token", async () => {
          expect(troll.burn(2)).to.be.reverted;
        });
        it("should allow owner to burn the token", async () => {
          expect(await troll.ownerOf(2)).to.be.eq(userA.address);
          const burnTx = await troll.connect(userA).burn(2);
          await burnTx.wait();
          const balance = await troll.balanceOf(userA.address);
          expect(Number(balance)).to.equal(3);
        });
      });
      describe("#totalSupply()", () => {
        it("should return the correct amount", async () => {
          expect(await troll["totalSupply()"]()).to.equal(6);
        });
      });
      describe("#batch()", () => {
        it("should not allow non-owner or non-operator to batch mint", async () => {
          const recipients = Array(10).fill(userA.address);
          const tokenURIS = Array(10).fill(TOKEN_CID);
          expect(troll.connect(userA).batchMint(recipients, tokenURIS)).to.be
            .reverted;
        });
        it("should allow owner to batch mint", async () => {
          const recipients = Array(10).fill(userA.address);
          const tokenURIS = Array(10).fill(TOKEN_CID);
          expect(troll.batchMint(recipients, tokenURIS)).to.not.be.reverted;
        });
        it("should not allow owner to batch mint with empty CDI", async () => {
          expect(troll.batchMint([], [])).to.be.revertedWith(
            "Error: Token URIs are empty!"
          );
        });
        it("should return the correct amount", async () => {
          expect(await troll["totalSupply()"]()).to.equal(16);
        });
      });
    });

    describe("Mint old and new colletion", () => {
      describe("#addCollection()", () => {
        it("should allow admin to add a new collection", async () => {
          expect(troll.connect(admin).addCollection("Wurstfinger")).to.not.be
            .reverted;
        });
        it("should allow any to mint old", async () => {
          const mint = await troll.connect(owner).mint(1, owner.address);
          await mint.wait();
          expect(await troll.ownerOf(17)).to.be.equal(owner.address);
        });
        it("should allow admin to send URIs", async () => {
          const addURIs = await troll
            .connect(owner)
            .addURIs(2, Array(10).fill(TOKEN_CID));
          await addURIs.wait();
          expect(await troll["totalSupply(uint256)"](2)).to.be.equal(10);
        });
        it("should allow owner to mint new", async () => {
          const mint = await troll.connect(owner).mint(2, owner.address);
          await mint.wait();
          expect(await troll.ownerOf(18)).to.be.equal(owner.address);
        });
        it("should allow user to mint new", async () => {
          const mint = await troll.connect(userA).mint(2, userA.address);
          await mint.wait();
          expect(await troll.ownerOf(19)).to.be.equal(userA.address);
        });
      });
    });

    describe("Upgrading to V2", () => {
      before(async () => {
        const MockContract = await ethers.getContractFactory("MockTrollToken");
        mockTroll = await upgrades.upgradeProxy(troll, MockContract);
      });
      describe("#burn()", () => {
        it("should allow owner to burn the token", async () => {
          await mockTroll.connect(userA).burn(3);
          const balance = await mockTroll.balanceOf(userA.address);
          expect(Number(balance)).to.eq(13);
        });
      });
    });
  });
});
