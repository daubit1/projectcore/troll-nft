import { Contract } from "ethers";
import { ethers } from "hardhat";
const { ABI, TOKEN_CDI } = require("./const");

function deployer(contract_address: string) {
  const contract = new ethers.Contract(ABI, contract_address);
  return contract;
}

async function mint(contract: Contract, from: string) {
  return await contract.methods
    .mintTo(from, TOKEN_CDI)
    .send(
      { from: from, gas: 1000000 },
      async (error: any, transactionHash: string) => {
        let transactionReceipt = null;
        while (transactionReceipt == null) {
          // Waiting expectedBlockTime until the transaction is mined
          transactionReceipt = await ethers.provider.getTransactionReceipt(
            transactionHash
          );
          await sleep(1000);
        }
      }
    );
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

module.exports = {
  mint,
  deployer,
};

// async function fetchContractData(creature, proxy) {
//   const owner_creature = await creature.methods
//     .owner()
//     .call({ from: OWNER_ADDRESS });

//   const owner = await proxy.methods.owner().call({ from: OWNER_ADDRESS });

//   const implementation = await proxy.methods
//     .implementation()
//     .call({ from: ADMIN_ADDRESS });
//   const admin = await proxy.methods.admin().call({ from: ADMIN_ADDRESS });

//   console.log(
//     `Address of owner when calling Creature contract: ${owner_creature}\n`,
//     `Address of owner when calling Creature contract: ${owner}\n`,
//     `Address of the logic contract: ${implementation}\n`,
//     `Address of the admin of the proxy contract: ${admin}\n`
//   );
// }
